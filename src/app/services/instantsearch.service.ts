import { Injectable } from '@angular/core';
import instantsearch from 'instantsearch.js/es';

@Injectable()
export class InstantSearchService {
  search = instantsearch({
    appId: '',
    apiKey: '',
    indexName: '',
    urlSync: true
  });

  constructor() {}
}