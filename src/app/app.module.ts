import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { InstantSearchService } from './services/instantsearch.service';
import { HitsComponent } from './components/hits/hits.component';
import { SearchBoxComponent } from './components/search-box/search-box.component';
import { RefinementListComponent } from './components/refinement-list/refinement-list.component';
import { PaginationComponent } from './components/pagination/pagination.component'

import * as $ from 'jquery';

@NgModule({
  declarations: [
    AppComponent,
    HitsComponent,
    SearchBoxComponent,
    RefinementListComponent,
    PaginationComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [InstantSearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
