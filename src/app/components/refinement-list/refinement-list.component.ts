import { Component, OnInit } from '@angular/core';
import { connectRefinementList } from 'instantsearch.js/es/connectors';

import { InstantSearchService } from '../../services/instantsearch.service';

@Component({
  selector: 'app-refinement-list',
  template: `
    <div>
      <label
        *ngFor="let item of (state.items ? state.items.slice(0, 10) : [])"
        (click)="handleChange($event.target.value)"
      >
        <input
          type="checkbox"
          [value]="item.value"
          [checked]="item.isRefined"
        />
        <span>({{ item.count }})</span>
      </label>
    </div>
  `,
  styles: []
})
export class RefinementListComponent implements OnInit {

  state: { query: string; refine: Function } = {
    query: '',
    refine() {}
  };

  constructor(private instantSearchService: InstantSearchService) {}

  ngOnInit() {
    const widget = connectRefinementList(this.updateState);
    this.instantSearchService.search.addWidget(widget({
      attributeName: 'typeFilter'
    }));
  }

  updateState = (state, isFirstRendering) => {
    // asynchronous update of the state
    // avoid `ExpressionChangedAfterItHasBeenCheckedError`
    if (isFirstRendering) {
      return Promise.resolve(null).then(() => {
        this.state = state;
      });
    }

    this.state = state;
  }

  handleChange(query) {
    this.state.refine(query);
  }
}
