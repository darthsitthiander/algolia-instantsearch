import { Component, OnInit } from '@angular/core';
import { InstantSearchService } from '../../services/instantsearch.service';
import { connectPagination } from 'instantsearch.js/es/connectors';

@Component({
  selector: 'app-pagination',
  template: `
    <div class="is-pagination-root">
      <ul>
        <li
          *ngFor="let page of pages"
          (click)="state.refine(page)"
          [ngClass]="{'active': page === state.currentRefinement}"
        >
          {{page + 1}}
        </li>
      </ul>
    </div>
  `,
  styles: [`
    ul, li {
      margin: 0;
      padding: 0;
      display: inline-block;
    }

    li {
      cursor: pointer;
      margin-right: 5px;
    }

    li.active {
      color: red;
      font-weight: bold;
    }
  `]
})
export class PaginationComponent implements OnInit {

  state: {
    currentRefinement: number,
    nbPages: number,
    refine: Function
  } = {
    currentRefinement: 0,
    nbPages: 0,
    refine: () => {}
  };

  constructor(private instantSearchService: InstantSearchService) {}

  get pages(): number[] {

    return Array
      .apply(null, {length: this.state.nbPages})
      .map(Number.call, Number)
  }

  ngOnInit() {
    const widget = connectPagination(this.updateState);
    this.instantSearchService.search.addWidget(widget({ maxPages: 10 }));
  }

  updateState = (state, isFirstRendering) => {
    if (isFirstRendering) {
      return Promise.resolve(null).then(() => {
        this.state = state;
      })
    }

    this.state = state;
  }
}
